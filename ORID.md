# Daily Report(07/10)
	Objective：了解了高效的学习方式，PDCA循环管理，如何制作concept-maps，还进行了两真一假的破冰活动。
	Reflective：受益匪浅
	Interpretive：两真一假的破冰活动让我对我的同学有了更深入的了解，PDCA循环管理可以运用在我阅读一些书籍的时候更高效，concept-maps可以帮助我将新的概念同化到已有的认知结构中，增强记忆。
	Decisional：尝试将PDCA运用于日常的阅读中，或者运用于平时对于工作的复盘，在学习到新知识的时候可以制作concept-maps，与我已经拥有的旧的知识体系形成联系，有助于加强记忆。
